## THRIFT-RPC-SERVER

利用springAutoConfiguration的原理在项目启动过程中引入一个thrift rpc server

## 使用

1.引入依赖：
```java
<dependency>
    <groupId>com.lowdad.thrift</groupId>
    <artifactId>server</artifactId>
    <version>1.0.1</version>
</dependency>
```
2.编写thrift IDL并生成java文件

```thrift
service RpcService {
    string Ping(1:string addr)
}
```
3.java实现thrift接口，这里demo定义为RpcHandler

```java
@Service
@Slf4j
public class RpcHandler implements RpcService.Iface {

    @Autowired
    private PingService pingService;

    @Override
    public String Ping(String addr) throws TException {
        log.info("called");
        return pingService.Ping(addr);
    }
}
```
service实现：

```java
@Service
@Slf4j
public class PingServiceImpl implements PingService{
    @Override
    public String Ping(String addr) {
        return "Hello "+addr;
    }
}
```

4.在springboot扫描包路径下新建config类实现rpc自动注入

```java
@Configuration
@ConditionalOnProperty(name = "rpc.enabled",matchIfMissing = true)
public class RPCServer {

    @Bean(name = "rpcProcessor")
    public TProcessor processor(RpcHandler handler) {
        return new RpcService.Processor<>(handler);
    }
}
```

5.可用的配置

```yaml
rpc:
  server:
    port: 9000
    connectTimeout: 5000
    backlog: 5000
    threadPool:
      coreSize: 64
      maxSize: 256
      keepAlive: 60
    selector: 16
    maxFrameSize: 16777216
    maxReadBufSize: 4194304
```

启动后就可以看到日志打印啦！rpc server已启动！

项目中加入了trace的依赖，需要结合logback才能打印traceId,参照trace项目中的介绍

## License MIT

