package com.lowdad.thrift.server;

import com.lowdad.thrift.server.config.TraceBinaryProtocol.Factory;
import com.lowdad.thrift.server.config.ServerConfigurationProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.thrift.TProcessor;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadedSelectorServer;
import org.apache.thrift.transport.*;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author javawcy
 */
@Slf4j
@EnableConfigurationProperties(ServerConfigurationProperties.class)
public class ThriftServerRunnable implements Runnable {

    private static final Factory THRIFT_PROTOCOL_FACTORY = new Factory();

    private final ServerConfigurationProperties config;

    protected ExecutorService threadPool;

    protected TServer server;

    private TProcessor processor;

    public ThriftServerRunnable(TProcessor processor,ServerConfigurationProperties serverConfigurationProperties) {
        this.processor = processor;
        this.config = serverConfigurationProperties;
    }

    public TServer build() throws TTransportException {
        TNonblockingServerSocket.NonblockingAbstractServerSocketArgs socketArgs =
                new TNonblockingServerSocket.NonblockingAbstractServerSocketArgs();
        socketArgs.port(Integer.valueOf(config.getPort()));
        socketArgs.clientTimeout(Integer.valueOf(config.getConnectTimeout()));
        socketArgs.backlog(Integer.valueOf(config.getBacklog()));

        TNonblockingServerTransport transport = new TNonblockingServerSocket(socketArgs);

        threadPool =
                new ThreadPoolExecutor(Integer.valueOf(config.getThreadPool().getCoreSize()), Integer.valueOf(config.getThreadPool().getMaxSize()),
                        Integer.valueOf(config.getThreadPool().getKeepAlive()), TimeUnit.SECONDS,
                        new SynchronousQueue<>());

        TTransportFactory transportFactory = new TFramedTransport.Factory(Integer.valueOf(config.getMaxFrameSize()));
        TThreadedSelectorServer.Args args = new TThreadedSelectorServer.Args(transport)
                .selectorThreads(Integer.valueOf(config.getSelector()))
                .executorService(threadPool)
                .transportFactory(transportFactory)
                .inputProtocolFactory(THRIFT_PROTOCOL_FACTORY)
                .outputProtocolFactory(THRIFT_PROTOCOL_FACTORY)
                .processor(processor);

        args.maxReadBufferBytes = Integer.valueOf(config.getMaxReadBufSize());

        return new TThreadedSelectorServer(args);
    }

    @Override
    public void run() {
        try {
            server = build();
            log.info("[RPC] rpc server run at {}",config.getPort());
            log.info("[RPC] rpc server connTimeOut {}",config.getConnectTimeout());
            log.info("[RPC] rpc server backlog {}",config.getBacklog());
            log.info("[RPC] rpc server threadPool coreSize {}",config.getThreadPool().getCoreSize());
            log.info("[RPC] rpc server threadPool maxSize {}",config.getThreadPool().getMaxSize());
            log.info("[RPC] rpc server threadPool keepAlive {}",config.getThreadPool().getKeepAlive());
            log.info("[RPC] rpc server selector {}",config.getSelector());
            log.info("[RPC] rpc server maxFrameSize {}",config.getMaxFrameSize());
            log.info("[RPC] rpc server maxReadBufSize {}",config.getMaxReadBufSize());
            log.info("[RPC] rpc server build success");
            server.serve();
        } catch (Exception e) {
            log.error("[RPC] rpc server build fail");
            e.printStackTrace();
            throw new RuntimeException("[RPC] Start Thrift RPC Server Exception");
        }
    }

    public void stop() throws Exception {
        threadPool.shutdown();
        server.stop();
    }


}