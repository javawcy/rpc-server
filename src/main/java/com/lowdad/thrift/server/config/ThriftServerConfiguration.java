package com.lowdad.thrift.server.config;

import com.lowdad.thrift.server.ThriftServerRunnable;
import lombok.extern.slf4j.Slf4j;
import org.apache.thrift.TProcessor;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * @author javawcy
 */
@Configuration
@ConditionalOnBean(value = {TProcessor.class})
@EnableConfigurationProperties(ServerConfigurationProperties.class)
@Slf4j
public class ThriftServerConfiguration implements InitializingBean, DisposableBean {

    private static final int GRACEFUL_SHOWDOWN_SEC = 3;

    private final ServerConfigurationProperties serverConfigurationProperties;

    @Autowired
    private TProcessor processor;

    private ThriftServerRunnable thriftServer;

    private Thread thread;

    public ThriftServerConfiguration(ServerConfigurationProperties serverConfigurationProperties) {
        this.serverConfigurationProperties = serverConfigurationProperties;
    }

    @Bean(name = "shutdownThriftServerRunnable")
    public Runnable shutdownThriftServerRunnable() {
        return () -> {
            log.info("[RPC] Shutdown thrift server.");
            try {
                thriftServer.stop();
            } catch (Exception e) {
                log.error("[RPC] Shutdown thrift server error", e);
            }
        };
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        thriftServer = new ThriftServerRunnable(processor,serverConfigurationProperties);
        thread = new Thread(thriftServer);
        thread.start();
    }

    @Override
    public void destroy() throws Exception {
        log.info("[RPC] Wait for graceful shutdown on destroy(), {} seconds", GRACEFUL_SHOWDOWN_SEC);
        Thread.sleep(TimeUnit.SECONDS.toMillis(GRACEFUL_SHOWDOWN_SEC));
        log.info("[RPC] Shutdown rpc server.");
        thriftServer.stop();
        thread.join();
    }
}
