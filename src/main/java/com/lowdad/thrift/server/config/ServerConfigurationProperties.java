package com.lowdad.thrift.server.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author javawcy
 */
@ConfigurationProperties(prefix = "rpc.server")
public class ServerConfigurationProperties {
    /**
     * 服务监听端口
     */
    private String port;
    /**
     * 服务连接超时
     */
    private String connectTimeout;
    /**
     * 服务队列长度
     */
    private String backlog;
    /**
     * 事件驱动线程数量
     */
    private String selector;
    /**
     * 最大传输流大小
     */
    private String maxFrameSize;
    /**
     * 最大读取大小
     */
    private String maxReadBufSize;
    private ThreadPoolProperties threadPool;

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(String connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public String getBacklog() {
        return backlog;
    }

    public void setBacklog(String backlog) {
        this.backlog = backlog;
    }

    public String getSelector() {
        return selector;
    }

    public void setSelector(String selector) {
        this.selector = selector;
    }

    public String getMaxFrameSize() {
        return maxFrameSize;
    }

    public void setMaxFrameSize(String maxFrameSize) {
        this.maxFrameSize = maxFrameSize;
    }

    public String getMaxReadBufSize() {
        return maxReadBufSize;
    }

    public void setMaxReadBufSize(String maxReadBufSize) {
        this.maxReadBufSize = maxReadBufSize;
    }

    public ThreadPoolProperties getThreadPool() {
        return threadPool;
    }

    public void setThreadPool(ThreadPoolProperties threadPool) {
        this.threadPool = threadPool;
    }
}
