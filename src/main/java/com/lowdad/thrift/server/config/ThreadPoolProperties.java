package com.lowdad.thrift.server.config;

/**
 * @author javawcy
 */
public class ThreadPoolProperties {
    /**
     * 常驻线程
     */
    private String coreSize;
    /**
     * 最大线程
     */
    private String maxSize;
    /**
     * 线程最大保留时间
     */
    private String keepAlive;

    public String getCoreSize() {
        return coreSize;
    }

    public void setCoreSize(String coreSize) {
        this.coreSize = coreSize;
    }

    public String getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(String maxSize) {
        this.maxSize = maxSize;
    }

    public String getKeepAlive() {
        return keepAlive;
    }

    public void setKeepAlive(String keepAlive) {
        this.keepAlive = keepAlive;
    }
}
